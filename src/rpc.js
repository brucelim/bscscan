module.exports = function (host, params) {
    const base_url = '/api?'
    const query = new URLSearchParams(params).toString();

    const url = `${host}/api?${query}`

    return fetch(url)
        .then(res => res.json())
        .catch(error => error)
}