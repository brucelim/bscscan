require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns the current amount of BNB in circulation.
 * @returns {Promise<string>}
 */
const getTotalBNBSupply = () => {
    return rpc(host, {
        module: "stats",
        action: "bnbsupply",
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the top 21 validators for the BNB Smart Chain.
 * @returns {Promise<string>}
 */
const getValidators = () => {
    return rpc(host, {
        module: "stats",
        action: "validators",
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the latest price of 1 ETH.
 * @returns {Promise<object>}
 */
const getBNBLastPrice = () => {
    return rpc(host, {
        module: "stats",
        action: "bnbprice",
        apikey
    }).then(response => {
        return response.result;
    })
}


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;
    
    return {
        getTotalBNBSupply,
        getValidators,
        getBNBLastPrice,
    }
}
