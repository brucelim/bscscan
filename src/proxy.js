require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns the number of most recent block
 * @returns {Promise<object>}
 */
const getBlockNumber = () => {
    return rpc(host, {
        module:  "proxy",
        action:  "eth_blockNumber",
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns information about a block by block number.
 * @param {number} blockno
 * @param {object} [options]
 * @param {boolean} [options.boolean]
 * @returns {Promise<object>}
 */
const getBlockByNumber = (blockno, options={}) => {
    const { boolean='true' } = options

    return rpc(host, {
        module:  "proxy",
        action:  "eth_getBlockByNumber",
        tag: '0x' + blockno.toString(16),
        boolean,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the number of transactions in a block.
 * @param {number} blockno
 * @returns {Promise<number>}
 */
const getBlockTransactionCount = (blockno) => {
    return rpc(host, {
        module: "proxy",
        action: "eth_getBlockTransactionCountByNumber",
        tag: web3.utils.numberToHex(blockno),
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns information about a transaction requested by transaction hash.
 * @param {string} txhash
 * @returns {Promise<object>}
 */
const getTransactionByHash = (txhash) => {
    return rpc(host, {
        module: "proxy",
        action: "eth_getTransactionByHash",
        txhash,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the number of transactions performed by an address.
 * @param {string} address
 * @param {object} [options]
 * @param {string} [options.tag=latest]
 * @returns {Promise<number>}
 */
const getTransactionCount = (address, options = {}) => {
    const { tag='latest' } = options

    return rpc(host, {
        module: "proxy",
        action: "eth_getTransactionCount",
        address,
        tag,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Submits a pre-signed transaction for broadcast to the BNB Smart Chain network.
 * @param {string} hex
 * @return {Promise<void>}
 */
const sendRawTransaction = async (hex) => {
    return {
        "msg": "not implemented yet"
    };
}

/**
 * Returns the receipt of a transaction that has been validated.
 * @param {string} txhash
 * @returns {Promise<object>}
 */
const getTransactionReceipt = (txhash) => {
    return rpc(host, {
        module: "proxy",
        action: "eth_getTransactionReceipt",
        txhash,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Executes a new message call immediately without creating a transaction on the block chain.
 * @param {string} to
 * @param {string} data
 * @param {object} [options]
 * @param {string} [options.tag=latest]
 * @returns {Promise<string>}
 */
const call = (to, data, options = {}) => {
    const { tag='latest' } = options

    return rpc(host, {
        module: "proxy",
        action: "eth_call",
        to,
        data,
        tag,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns code at a given address.
 * @param {string} address
 * @param {object} [options]
 * @param {string} [options.tag=latest]
 * @returns {Promise<string>}
 */
const getCode = (address, options = {}) => {
    const { tag='latest' } = options

    return rpc(host, {
        module: "proxy",
        action: "eth_getCode",
        address,
        tag,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the value from a storage position at a given address.
 * This endpoint is still experimental and may have potential issues
 * @param {string} address
 * @param {number} position
 * @param {object} options
 * @param {string} [options.tag=latest]
 * @returns {Promise<string>}
 */
const getStorageAt = (address, position, options = {}) => {
    const { tag='latest' } = options

    return rpc(host, {
        module: "proxy",
        action: "eth_getCode",
        address,
        position: web3.utils.numberToHex(position),
        tag,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the current price per gas in wei.
 * @param {object} [options]
 * @param {string} [options.unit=wei]
 * @returns {Promise<string>}
 */
const getGasPrice = (options={}) => {
    const { unit='wei' } = options

    return rpc(host, {
        module: "proxy",
        action: "eth_gasPrice",
        apikey
    }).then(price => {
        if (unit === 'wei') {
            return price
        }

        return web3.utils.fromWei(price, unit)
    }).then(response => {
        return response.result;
    })
}

/**
 * Makes a call or transaction, which won't be added to the blockchain and returns the gas used.
 * @param {string} to
 * @param {object} options
 * @param {string} options.value
 * @param {string} options.gasPrice
 * @param {string} options.gas
 * @return {Promise<void>}
 */
const estimateGas = (to, { data, value, gasPrice, gas }) => {
    return rpc(host, {
        module: "proxy",
        action: "eth_estimateGas",
        to,
        data,
        value,
        gasPrice,
        gas,
        apikey
    }).then(response => {
        return response.result;
    })
}


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;
    
    return {
        getBlockNumber,
        getBlockByNumber,
        getBlockTransactionCount,
        getTransactionByHash,
        getTransactionCount,
        sendRawTransaction,
        getTransactionReceipt,
        call,
        getCode,
        getStorageAt,
        getGasPrice,
        estimateGas,
    }
}
