require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns the current circulating supply of a BEP-20 token.
 * @param {string} contractaddress
 * @returns {Promise<string>}
 */
const getTokenCirculatingSupplyByContractAddress = (contractaddress) => {
    return rpc(host, {
        module: "stats",
        action: "tokenCsupply",
        contractaddress,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the total supply of a BEP-20 token.
 * @param {string} contractaddress
 * @returns {Promise<string>}
 */
const getTokenTotalSupplyByContractAddress = (contractaddress) => {
    return rpc(host, {
        module: "stats",
        action: "tokensupply",
        contractaddress,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the current balance of a BEP-20 token of an address.
 * @param {string} contractaddress
 * @param {string} address
 * @param {object} [options]
 * @param {object} [options.tag=latest]
 * @returns {Promise<string>}
 */
const getTokenBalanceByContractAddress = (contractaddress, address, options = {}) => {
    const { tag='latest' } = options

    return rpc(host, {
        module: "account",
        action: "tokenbalance",
        contractaddress,
        address,
        tag,
        apikey
    }).then(response => {
        return response.result;
    })
}


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;
    
    return {
        getTokenCirculatingSupplyByContractAddress,
        getTokenTotalSupplyByContractAddress,
        getTokenBalanceByContractAddress,
    }
}
