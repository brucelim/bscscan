require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns the status code of a transaction execution.
 * @param {string} txhash
 * @returns {Promise<object>}
 */
const getContractExecutionStatus = (txhash) => {
    return rpc(host, {
        module: "transaction",
        action: "getstatus",
        txhash,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * 
 */
const checkTransactionReceiptStatus = getContractExecutionStatus;


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;
    
    return {
        checkTransactionReceiptStatus,
        getContractExecutionStatus,
    }
}
