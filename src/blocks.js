require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns the block reward awarded for validating a certain block.
 * @param {number} blockno
 * @returns {Promise<object>}
 */
const getBlockReward = (blockno) => {
    return rpc(host, {
        module:  "block",
        action:  "getblockreward",
        blockno,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the estimated time remaining, in seconds, until a certain block is validated.
 * @param {number} blockno
 * @returns {Promise<object>}
 */
const getBlockCountDownTime = (blockno) => {
    return rpc(host, {
        module:  "block",
        action:  "getblockcountdown",
        blockno,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the block number that was validated at a certain timestamp.
 * @param {number} timestamp
 * @param {object} [options]
 * @param {string|number} [options.closest]
 * @returns {Promise<object>}
 */
const getBlockByTimestamp = (timestamp, options={}) => {
    const { closest='before' } = options

    return rpc(host, {
        module:  "block",
        action:  "getblocknobytime",
        timestamp,
        closest,
        apikey
    }).then(response => {
        return response.result;
    })
}


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;
    
    return {
        getBlockReward,
        getBlockCountDownTime,
        getBlockByTimestamp,
    }
}
