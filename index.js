'use strict'

function getHost(network) {
    switch (network) {
        case 'mainnet': return 'https://api.bscscan.com';
        case 'testnet': return 'https://api-testnet.bscscan.com';
        default: return 'https://api.bscscan.com';
    }
}

module.exports = function (config) {
    this.web3 = require('web3')
    config.host = getHost(config.network);
    console.log(`Running on: ${config.host}`)

    this.accounts = require('./src/accounts')(config)
    this.contracts = require('./src/contracts')(config)
    this.transactions = require('./src/transactions')(config)
    this.blocks = require('./src/blocks')(config)
    this.logs = require('./src/logs')(config)
    this.proxy = require('./src/proxy')(config)
    this.tokens = require('./src/tokens')(config)
    this.gastracker = require('./src/gastracker')(config)
    this.stats = require('./src/stats')(config)
}