require('dotenv').config()

const test = require('tape');
const _bscscan = require('../index');
const { transactions } = new _bscscan({
    token: process.env.BSCSCAN_APIKEY,
    network: "mainnet"
})


test('[03|01] Check Transaction Receipt Status', async function(t) {
    const status = await transactions.checkTransactionReceiptStatus('0xe9975702518c79caf81d5da65dea689dcac701fcdd063f848d4f03c85392fd00');
    const compare = { isError: '0', errDescription: '' };

    t.ok(status)
    t.deepEqual(status, compare)
    t.end()
})
