require('dotenv').config()

const test = require('tape');
const _bscscan = require('../index');
const { gastracker, web3 } = new _bscscan({
    token: process.env.BSCSCAN_APIKEY,
    network: "mainnet"
})


test('[08|01] Get Gas Oracle', async function(t) {
    const gasprice = await gastracker.getGasOracle()

    t.ok(gasprice)
    t.is(typeof gasprice, 'object')
})
