require('dotenv').config()

const test = require('tape');
const _bscscan = require('../index');
const { tokens, web3 } = new _bscscan({
    token: process.env.BSCSCAN_APIKEY,
    network: "mainnet"
})

/**
 * circulating supply number might change
 */
test('[07|01] Get BEP-20 Token CirculatingSupply by ContractAddress', async function(t) {
    const contractaddress = '0xe9e7cea3dedca5984780bafc599bd69add087d56'
    const circulating_supply = await tokens.getTokenCirculatingSupplyByContractAddress(contractaddress)

    t.ok(circulating_supply)
    // t.is(circulating_supply, '4752944115615746889279490759')
})

/**
 * total supply number should remain same??
 */
test('[07|02] Get BEP-20 Token TotalSupply by ContractAddress', async function(t) {
    const contractaddress = '0xe9e7cea3dedca5984780bafc599bd69add087d56'
    const total_supply = await tokens.getTokenTotalSupplyByContractAddress(contractaddress)

    t.ok(total_supply)
    t.is(total_supply, '4850999126519409465655005310')
})

test('[07|02] Get BEP-20 Token Account Balance by ContractAddress', async function(t) {
    const contractaddress = '0xe9e7cea3dedca5984780bafc599bd69add087d56'
    const address = '0x89e73303049ee32919903c09e8de5629b84f59eb'
    const token_balance = await tokens.getTokenBalanceByContractAddress(contractaddress, address)

    t.ok(token_balance)
    t.is(token_balance, '1000000000000000000')
})
