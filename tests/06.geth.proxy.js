require('dotenv').config()

const test = require('tape');
const _bscscan = require('../index');
const { proxy, web3 } = new _bscscan({
    token: process.env.BSCSCAN_APIKEY,
    network: "mainnet"
})


test('[06|01] Proxy eth_blockNumber', async function(t) {
    const blockno = await proxy.getBlockNumber()

    t.ok(blockno)
    t.is(web3.utils.isHexStrict(blockno), true)
    t.is(typeof web3.utils.hexToNumber(blockno), 'number')
})

test('[06|02] Proxy eth_getBlockByNumber', async function(t) {
    const blockno = '0xa11446'
    const number = web3.utils.hexToNumber(blockno)
    const block = await proxy.getBlockByNumber(number)

    t.ok(block)
    t.is(block.transactions.length, 252)
    t.is(block.number, blockno)
})

test('[06|03] eth_getBlockTransactionCountByNumber', async function(t) {
    const blockno = '0xa11446'
    const number = web3.utils.hexToNumber(blockno)
    const transaction_count = await proxy.getBlockTransactionCount(number)

    t.ok(transaction_count)
    t.is(web3.utils.isHexStrict(transaction_count), true)
    t.is(web3.utils.hexToNumber(transaction_count), 252)
})

test('[06|04] Proxy eth_getTransactionByHash', async function(t) {
    const txhash = '0x9983332a52df5ad1dabf8fa81b1642e9383f302a399c532fc47ecb6a7a967166'
    const transaction = await proxy.getTransactionByHash(txhash)

    t.ok(transaction)
    t.is(transaction.blockNumber, '0xa11595')
    t.is(transaction.hash, txhash)
})

test('[06|05] Proxy eth_getTransactionCount', async function(t) {
    const address = '0x4430b3230294D12c6AB2aAC5C2cd68E80B16b581'
    const transaction_count = await proxy.getTransactionCount(address)

    t.ok(transaction_count)
    t.is(web3.utils.isHexStrict(transaction_count), true)
})

test('[06|06] Proxy eth_sendRawTransaction', async function(t) {
    const result = await proxy.sendRawTransaction('transaction hexcode')
    const compare = { msg: 'not implemented yet' }

    t.deepEqual(result, compare)
})

test('[06|07] Proxy eth_getTransactionReceipt', async function(t) {
    const txhash = '0x2122b2317d6cf409846f80e829c1e45ecb30306907ba0a00a02730c78890739f'
    const receipt = await proxy.getTransactionReceipt(txhash)

    t.ok(receipt)
    t.is(receipt.transactionHash, txhash)
})

test('[06|08] Proxy eth_call', async function(t) {
    const to = '0xAEEF46DB4855E25702F8237E8f403FddcaF931C0'
    const data = '0x70a08231000000000000000000000000e16359506c028e51f16be38986ec5746251e9724'
    const result = await proxy.call(to, data)

    t.ok(result)
    t.is(web3.utils.isHexStrict(result), true)
    t.is(web3.utils.hexToNumber(result), 0)
})

/**
 * @TODO better assertions
 * code comes back as hex string
 */
test('[06|09] Proxy eth_getCode', async function(t) {
    const address = '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82'
    const code = await proxy.getCode(address)

    t.ok(code)
})

/**
 * @TODO better assertions
 * msg comes back as hex string
 */
test('[06|10] Proxy eth_getStorageAt', async function(t) {
    const address = '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82'
    const position = '0x0'
    const msg = await proxy.getStorageAt(address, position)

    t.ok(msg)
})

test('[06|11] Proxy eth_gasPrice', async function(t) {
    const gasprice = await proxy.getGasPrice();

    t.ok(gasprice)
    t.is(web3.utils.isHexStrict(gasprice), true)
})

test('[06|12] Proxy eth_estimateGas', async function(t) {
    const address = '0xEeee7341f206302f2216e39D715B96D8C6901A1C'
    const data = '0x4e71d92d'
    const value = '0xff22'
    const gasPrice = '0x51da038cc'
    const gas = '0x5f5e0ff'
    const estimate = await proxy.estimateGas(address, { data, value, gasPrice, gas })

    t.ok(estimate)
    t.is(web3.utils.isHexStrict(estimate), true)
    t.is(web3.utils.hexToNumber(estimate), 21064)
})
