require('dotenv').config()

const test = require('tape');
const _bscscan = require('../index');
const { stats, web3 } = new _bscscan({
    token: process.env.BSCSCAN_APIKEY,
    network: "mainnet"
})

/**
 * total supply increase over time
 * inflating token, hehe
 */
test('[09|01] Get Total Supply of BNB on the BNB Smart Chain', async function(t) {
    const bnb_supply = await stats.getTotalBNBSupply()

    t.ok(bnb_supply)
    // t.is(bnb_supply, '22437504870016300000000000')
})

test('[09|02] Get Validators List on the BNB Smart Chain', async function(t) {
    const validators = await stats.getValidators()

    t.ok(validators)
    t.is(validators.length, 21)
})

test('[09|03] Get BNB Last Price', async function(t) {
    const bnb_price = await stats.getBNBLastPrice()

    t.ok(bnb_price)
    t.isNot(Object.keys(bnb_price).indexOf('ethbtc'), -1)
})
