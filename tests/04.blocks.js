require('dotenv').config()

const test = require('tape');
const _bscscan = require('../index');
const { blocks } = new _bscscan({
    token: process.env.BSCSCAN_APIKEY,
    network: "mainnet"
})


test('[04|01] Get Block Rewards by BlockNo', async function(t) {
    const reward = await blocks.getBlockReward(2170000)
    const compare = {
        blockNumber: '2170000',
        timeStamp: '1605182836',
        blockMiner: '0x68bf0b8b6fb4e317a0f9d6f03eaf8ce6675bc60d',
        blockReward: '13657915000000000',
        uncles: [],
        uncleInclusionReward: '0'
    }

    t.ok(reward)
    t.deepEqual(reward, compare)
    t.end()
});

test('[04|02] Get Estimated Block Countdown Time by BlockNo', async function(t) {
    const countdown_time = await blocks.getBlockCountDownTime(51926850);

    t.ok(countdown_time)
    t.equal(countdown_time.CountdownBlock, '51926850')
})

test('[04|03] Get Block Number by Timestamp', async function(t) {
    const blockno = await blocks.getBlockByTimestamp(1601510400)

    t.ok(blockno)
    t.equal(blockno, '946206')
})
