require('dotenv').config()

const test = require('tape')
const _bscscan = require('../index');
const bscscan = new _bscscan({
    token: process.env.BSCSCAN_APIKEY,
    network: "mainnet"
})

test('00 bscscan module setup', (t) => {
    t.equals(typeof _bscscan, 'function')
    t.equals(typeof bscscan, 'object')
    t.ok(bscscan.web3)
    t.ok(bscscan.accounts)
    t.ok(bscscan.contracts)
    t.ok(bscscan.transactions)
    t.ok(bscscan.blocks)
    // t.ok(bscscan.logs)
    t.ok(bscscan.proxy)
    t.ok(bscscan.tokens)
    t.ok(bscscan.gastracker)
    t.ok(bscscan.stats)

    t.end()
});
