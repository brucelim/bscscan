require('dotenv').config()

const test = require('tape');
const _bscscan = require('../index');
const { contracts } = new _bscscan({
    token: process.env.BSCSCAN_APIKEY,
    network: "mainnet"
})


test('[02|01] Get Contract ABI for Verified Contract Source Codes', async function(t) {
    const abi = await contracts.getContractAbi('0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82');

    t.ok(abi)

    const approval = abi.shift();
    t.equal(approval.name, 'Approval');
    t.end()
})

test('[02|02] Get Contract Source Code for Verified Contract Source Codes', async function(t) {
    const response = await contracts.getContractSource('0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82');
    
    t.ok(response)

    const source = response.shift()

    t.equal(source.ContractName, 'CakeToken')
    t.end()
})
