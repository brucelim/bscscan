require('dotenv').config()

const test = require('tape');
const _bscscan = require('../index');
const { accounts } = new _bscscan({
    token: process.env.BSCSCAN_APIKEY,
    network: "mainnet"
})


test('[01|01] Get BNB Balance for a Single Address', async function(t) {
    t.plan(2)

    const balance = await accounts.getAccountBalance('0x4281eCF07378Ee595C564a59048801330f3084eE')
    t.ok(balance)
    t.equal(typeof balance, 'string')
    t.end()
})

test('[01|02] Get BNB Balance for Multiple Addresses in a Single Call', async function(t) {
    const addresses = [ 
        '0x3f349bBaFEc1551819B8be1EfEA2fC46cA749aA1',
        '0x83EBba62B9232f2a6cB16576Af9D48b349D1816d',
        '0x70F657164e5b75689b64B7fd1fA275F334f28e18'
    ];
    
    const balances = await accounts.getAccountBalances(addresses)
    t.ok(balances)
    t.equal(balances.length, 3)
    t.end()
})

test('[01|03] Returns the list of transactions performed by an address, with optional pagination.', async function(t) {
    const transactions = await accounts.getTransactions('0xF426a8d0A94bf039A35CEE66dBf0227A7a12D11e', {
        startblock: 0,
        page:1,
        offset:10
    })

    t.ok(transactions)
    t.equal(transactions.length, 10)
    t.end()
})

test('[01|04] Get a list of \'Internal\' Transactions by Address', async function(t) {
    const transactions = await accounts.getInternalTransactions('0x0000000000000000000000000000000000001004', {
        startblock: 0,
        endblock: 2702578,
        page: 1,
        offset: 10
    })

    t.ok(transactions)
    t.equal(transactions.length, 10)
    t.end()
})

test('[01|05] Get \'Internal Transactions\' by Transaction Hash', async function(t) {
    const transactions = await accounts.getInternalTransactionsByHash('0x4d74a6fc84d57f18b8e1dfa07ee517c4feb296d16a8353ee41adc03669982028')

    t.ok(transactions)
    t.equal(transactions.length, 2)
    t.end()
})

test('[01|06] Get "Internal Transactions" by Block Range', async function(t) {
    const transactions = await accounts.getInternalTransactionsByBlockRange({
        startblock: 0,
        endblock: 2702578,
        page: 1,
        offset: 10
    })

    t.ok(transactions)
    t.equal(transactions.length, 10)

    const tx = transactions.shift()

    t.equal(tx.blockNumber, '958')
    t.equal(tx.timeStamp, '1598674477')
    t.end()
})

test('[01|07] Get a list of \'BEP-20 Token Transfer Events\' by Address', async function(t) {
    const address = '0x7bb89460599dbf32ee3aa50798bbceae2a5f7f6a';
    const contractaddress = '0xc9849e6fdb743d08faee3e34dd2d1bc69ea11a51';
    const transfers = await accounts.getBEP20TokenTransferEventsByAddress(
        address,
        contractaddress,
        {
            startblock: 0,
            endblock: 999999999,
            page: 1,
            offset: 5
        }
    );

    t.ok(transfers)
    t.equal(transfers.length, 5)

    const tx = transfers.shift()

    t.equal(tx.blockNumber, '2304192')
    t.equal(tx.to, address);
    t.end()
})

test('[01|08] Get a list of \'BEP-721 Token Transfer Events\' by Address', async function(t) {
    const address = '0xcd4ee0a77e09afa8d5a6518f7cf8539bef684e6c';
    const contractaddress = '0x5e74094cd416f55179dbd0e45b1a8ed030e396a1';
    const transfers = await accounts.getBEP721TokenTransferEventsByAddress(
        address,
        contractaddress,
        {
            startblock: 0,
            endblock: 999999999,
            page: 1,
            offset: 5
        }
    );

    t.ok(transfers)
    t.equal(transfers.length, 5)

    const tx = transfers.shift()

    t.equal(tx.blockNumber, '2657100')
    t.equal(tx.to, address);
    t.end()
})

test('[01|09] Get list of Blocks Validated by Address', async function(t) {
    const address = '0x78f3adfc719c99674c072166708589033e2d9afe';
    // can also call getMinedBlocks
    const blocks = await accounts.getValidatedBlocks(address, {
        page: 1,
        offset: 10
    })

    const compare = {
        blockNumber: '5428195',
        timeStamp: '1614988862',
        blockReward: '106809335800031367'
    };

    t.ok(blocks)
    t.equal(blocks.length, 10)

    const block = blocks.shift()

    t.deepEquals(block, compare)
    t.end()
})

