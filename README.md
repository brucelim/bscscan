
# bscscan

node js bscscan api

#### api

[https://docs.bscscan.com/](https://docs.bscscan.com/)

```
- Accounts
    getAccountBalance,
    getAccountBalances,
    getTransactions,
    getInternalTransactions,
    getInternalTransactionsByHash,
    getInternalTransactionsByBlockRange,
    getBEP20TokenTransferEventsByAddress,
    getBEP721TokenTransferEventsByAddress,
    getMinedBlocks,
    getValidatedBlocks,

- Contracts
    getContractAbi,
    getContractSource,

- Transactions
    checkTransactionReceiptStatus,
    getContractExecutionStatus,

- Blocks
    getBlockReward,
    getBlockCountDownTime,
    getBlockByTimestamp,

- Logs
    getEventsLogs,

- Geth Proxy
    getBlockNumber,
    getBlockByNumber,
    getBlockTransactionCount,
    getTransactionByHash,
    getTransactionCount,
    sendRawTransaction,
    getTransactionReceipt,
    call,
    getCode,
    getStorageAt,
    getGasPrice,
    estimateGas,

- Gas Tracker
    getGasOracle,

- Stats
    getTotalBNBSupply,
    getValidators,
    getBNBLastPrice,
```
